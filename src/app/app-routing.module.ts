import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: '/client/order' }
];

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forRoot(routes, {useHash: true}),
    ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {
}
