import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ClientRoutingModule} from './client-routing.module';
import {OrderComponent} from './containers/order/order.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {SelectProductComponent} from './components/select-product/select-product.component';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';


@NgModule({
    declarations: [OrderComponent, SelectProductComponent],
    imports: [
        CommonModule,
        ClientRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatIconModule,
        MatTooltipModule
    ]
})
export class ClientModule {
}
