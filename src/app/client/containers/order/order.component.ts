import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Product} from '../../models/product';

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
    isOpen: boolean;
    orderForm: FormGroup;
    sampleProducts: Product[] = [
        {
            id: 'product-1',
            name: 'Product 1',
            description: 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis.',
        },
        {
            id: 'product-2',
            name: 'Product 2',
            description: 'Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Lorem ipsum dolor sit amet enim.',
        },
        {
            id: 'product-3',
            name: 'Product 3 with long name',
            description: 'Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Lorem ipsum dolor sit amet enim.',
        },
        {
            id: 'product-4',
            name: 'Product 4',
            description: 'Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Lorem ipsum dolor sit amet enim.',
        },
        {
            id: 'product-5',
            name: 'Product 5 with very long name a pellentesque dui non felis lorem ipsum',
            description: 'Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Lorem ipsum dolor sit amet enim.',
        },
        {
            id: 'product-6',
            name: 'Product 6',
            description: 'Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Lorem ipsum dolor sit amet enim.',
        }
    ];

    constructor(private fb: FormBuilder) {
        this.isOpen = true;
        this.orderForm = this.fb.group({
            products: [''],
        });
    }

    ngOnInit(): void {
    }

}
