import {Component, Input, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Product} from '../../models/product';

@Component({
  selector: 'app-select-product',
  templateUrl: './select-product.component.html',
  styleUrls: ['./select-product.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SelectProductComponent), multi: true }
  ]
})
export class SelectProductComponent implements ControlValueAccessor {
  @Input() products: Product[];
  value: Product[] = [];
  propagateChange: any = () => {};

  constructor() { }

  toggleState(product): void {
    if (this.value.includes(product)) {
      this.value = this.value.filter(item => item.id !== product.id);
    } else {
      this.value.push(product);
    }
    this.propagateChange(this.value);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value): void {
    if (value) {
      this.value = value;
    }
  }

  isActiveProduct(product): boolean {
    return this.value.includes(product);
  }

}
